from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, CreateAccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def receipt_list(request):
    list_of_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "list_of_receipts": list_of_receipts
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list
    }

    return render(request, "receipts/categories/list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts
    }

    return render(request, "receipts/accounts/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(commit=False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")

    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form
        }

    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account_instance = form.save(commit=False)
            account_instance.owner = request.user
            account_instance.save()
            return redirect("account_list")

    else:
        form = CreateAccountForm()

    context = {
        "form": form
        }

    return render(request, "receipts/accounts/create.html", context)
